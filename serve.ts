import * as express from 'express';
import * as webpack from 'webpack';
import graphqlServer from './schema';
import * as webpackConfig from './webpack.config';

const webpackCompiler = webpack(webpackConfig as webpack.Configuration);
console.log('building...');
webpackCompiler.run(() => {
  console.log('build done');
});

const app = express()
app.use(express.static(`${__dirname}/dist/`));
graphqlServer.applyMiddleware({ app });
app.use('/graphql', () => { })
app.use((req, res) => res.sendFile(`${__dirname}/dist/index.html`))

app.listen(Number(process.env.PORT) || 8000, process.env.HOST || '0.0.0.0', () => {
  console.log('listening on 0.0.0.0:8000');
});

