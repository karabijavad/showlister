'use strict';
module.exports = (sequelize, DataTypes) => {
  var Venue = sequelize.define('Venue', {
    ticketflyId: DataTypes.INTEGER,
    name: DataTypes.STRING,
    address: DataTypes.STRING,
    lat: DataTypes.DECIMAL,
    lng: DataTypes.DECIMAL
  }, {
      indexes: [
        {
          unique: true,
          fields: ['ticketflyId'],
        },
        { fields: ['lat', 'lng'], }
      ],
    });
  Venue.associate = function (models) {
    Venue.hasMany(models.Happening);
  };
  return Venue;
};
