'use strict';
module.exports = (sequelize, DataTypes) => {
  var Happening = sequelize.define('Happening', {
    ticketflyId: DataTypes.INTEGER,
    name: DataTypes.STRING,
    startDatetime: DataTypes.DATE,
    endDatetime: DataTypes.DATE,
  }, {
      indexes: [
        {
          unique: true,
          fields: ['ticketflyId'],
        },
      ],
    });
  Happening.associate = function (models) {
    Happening.belongsTo(models.Venue);
    Happening.belongsToMany(models.Artist, { through: 'HappeningArtist' });
  };

  return Happening;
};

