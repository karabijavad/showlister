'use strict';
module.exports = (sequelize, DataTypes) => {
  var Artist = sequelize.define('Artist', {
    ticketflyId: DataTypes.INTEGER,
    name: DataTypes.STRING,
  }, {
      indexes: [
        {
          unique: true,
          fields: ['ticketflyId'],
        },
        { fields: ['name'] }
      ]
    });
  Artist.associate = function (models) {
    Artist.belongsToMany(models.Happening, { through: 'HappeningArtist' });
  };
  return Artist;
};
