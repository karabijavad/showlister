FROM ubuntu:18.04

ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update &&\
  apt-get install --yes software-properties-common &&\
  add-apt-repository ppa:apt-fast/stable

RUN apt-get -y install apt-fast
RUN apt-fast install --yes curl

RUN curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add -
RUN add-apt-repository -y -r ppa:chris-lea/node.js
RUN apt-fast install -y build-essential
RUN apt-fast install -y nodejs npm
RUN npm install --global yarn

RUN useradd -ms /bin/bash showlister-www
USER showlister-www

RUN mkdir /home/showlister-www/showlister-www
WORKDIR /home/showlister-www/showlister-www

ADD --chown=showlister-www package.json /home/showlister-www/showlister-www
ADD --chown=showlister-www yarn.lock /home/showlister-www/showlister-www

RUN yarn install --dev

ADD --chown=showlister-www . /home/showlister-www/showlister-www
EXPOSE 8000
CMD ["yarn", "serve"]
