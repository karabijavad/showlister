import { ApolloServer, gql } from 'apollo-server-express';
import { resolver } from 'graphql-sequelize';
import * as models from '../models';

const typeDefs = gql`
  type Artist {
    id: Int!
    name: String,
  }

  type Venue {
    id: Int!
    name: String,
    address: String,
    lat: Float,
    lng: Float,
  }

  type Happening {
    id: Int!,
    name: String,
    venue: Venue,
    artists: [Artist],
  }

  type Query {
    artists: [Artist],
    happenings: [Happening],
    venues: [Venue],
  }
`;

const resolvers = {
  Query: {
    artists: resolver(models.Artist),
    venues: resolver(models.Venue),
    happenings: resolver(models.Happening),
  },
  Happening: {
    venue(happening) {
      return happening.getVenue();
    },
    artists(happening) {
      return happening.getArtists();
    }
  }
};

const server = new ApolloServer({ typeDefs, resolvers });

export default server;
