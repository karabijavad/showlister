import * as program from 'commander';
import pullTicketfly from './pullTicketfly';

program
  .command('pullTicketfly')
  .action(pullTicketfly);

program.parse(process.argv);
