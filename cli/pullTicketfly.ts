import axios from 'axios';
import * as models from '../models';
import * as moment from 'moment';
import 'moment-timezone';

const Artist = models.Artist;
const Happening = models.Happening;
const Venue = models.Venue;

const axiosInstance = axios.create({
  headers: {
    'User-Agent': "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.85 Safari/537.36",
  },
});

interface ITicketflyArtist {
  id: number,
  name: string,
}
interface ITicketflyVenue {
  id: number,
  name: string,
  timeZone: string,
}
interface ITicketflyEvent {
  id: number,
  name: string,
  venue: ITicketflyVenue,
  startDate: string,
  endDate: string,
  headliners: [ITicketflyArtist],
}

interface ITicketflyResponse {
  events: [ITicketflyEvent],
}

const pullTicketfly = () => {
  axiosInstance.get<ITicketflyResponse>("http://ticketfly.com/api/events/upcoming.json?orgId=1&maxResults=10")
    .then(({ data }) => {
      data.events.forEach((ticketflyEvent) => {
        const timezone = ticketflyEvent.venue.timeZone;
        const startDatetime = moment.tz(ticketflyEvent.startDate, timezone).utc();
        const endDatetime = moment.tz(ticketflyEvent.endDate, timezone).utc();

        Happening.findOrCreate({
          defaults: {
            endDatetime,
            name: ticketflyEvent.name,
            startDatetime,
          },
          where: { ticketflyId: ticketflyEvent.id },
        }).then((happenings: any) => {
          const happening = happenings[0];

          Venue.findOrCreate({
            defaults: { name: ticketflyEvent.venue.name },
            where: { ticketflyId: ticketflyEvent.venue.id },
          }).then((foundVenues: any) => {
            const venue = foundVenues[0];
            happening.setVenue(venue);
          });

          ticketflyEvent.headliners.map(headliner => {
            Artist.findOrCreate({
              defaults: { name: headliner.name },
              where: { ticketflyId: headliner.id },
            }).then((foundArtists: any) => {
              const artist = foundArtists[0];
              happening.addArtist(artist);
            });
          });
        });
      })
    });
}

export default pullTicketfly;
