import * as moment from "moment";
import * as React from 'react';
import { GoogleMap, InfoWindow, Marker, withGoogleMap, withScriptjs } from "react-google-maps";

interface IProps {
  venues: Array<{}>;
  onBoundsChanged?: (bounds: any) => {};
}

interface IState {
  selectedVenue: string | null;
}

class ShowMap extends React.Component<IProps, IState> {
  public state = {
    selectedVenue: null,
  }

  public googleMap: GoogleMap | null = null;

  constructor(props: any) {
    super(props);
    this.setGoogleMap = this.setGoogleMap.bind(this);
    this.onBoundsChanged = this.onBoundsChanged.bind(this);
  }

  public onBoundsChanged() {
    if (this.props.onBoundsChanged && this.googleMap) {
      this.props.onBoundsChanged(this.googleMap.getBounds());
    }
  }

  public setGoogleMap(googleMap: GoogleMap) {
    this.googleMap = googleMap;
  }


  public selectVenue(venueId: string) {
    this.setState({
      ...this.state,
      selectedVenue: venueId,
    })
  }

  public render() {
    return (
      <GoogleMap
        defaultZoom={14}
        defaultCenter={{ lat: 30.2672, lng: -97.7431 }}
        onBoundsChanged={this.onBoundsChanged}
        ref={this.setGoogleMap}
      >
        {(this.props.venues.map((venue: any) => {
          const venueEvents = venue.events.edges.map((edge: any) => edge.node);
          return (
            <Marker onClick={() => { this.selectVenue(venue.id) }} key={venue.id} position={{ lat: venue.lat, lng: venue.lng }}>
              {venue.id === this.state.selectedVenue && <InfoWindow>
                <div>
                  {venue.name}
                  <div>
                    <ul>
                      {venueEvents.map((event: any) => (
                        <li key={event.id}>
                          <b>{event.name}</b> - <i>{moment(event.startDatetime).fromNow()}</i>
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>
              </InfoWindow>}
            </Marker>
          );
        }))}
      </GoogleMap >
    );
  }
}

export default withScriptjs(withGoogleMap(ShowMap));
