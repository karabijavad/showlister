import { TextField } from '@material-ui/core';
import * as React from 'react';

interface IProps {
  onFiltersChanged: (filters: {}) => void;
}
interface IState {
  eventsArtistsNameIcontains?: string | null;
  eventsStartDatetimeGte?: string;
  eventsEndDatetimeLte?: string;
}

class ShowMapFilters extends React.Component<IProps, IState> {
  public state = {
    eventsArtistsNameIcontains: null,
  }

  public handleChange = (name: string) => (event: any) => {
    this.setState({
      ...this.state,
      [name]: event.target.value,
    }, () => this.props.onFiltersChanged(this.state));
  };

  public render() {
    return (
      <div>
        <TextField
          id="eventsArtistsNameIcontains"
          label="Artist Name"
          value={this.state.eventsArtistsNameIcontains || ""}
          onChange={this.handleChange('eventsArtistsNameIcontains')}
          margin="normal"
        />
      </div>
    );
  }
}

export default ShowMapFilters;
