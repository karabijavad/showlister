import gql from 'graphql-tag';
import { debounce } from 'lodash';
import * as moment from 'moment';
import * as React from 'react';
import { Query } from 'react-apollo';
import ShowMap from './ShowMap';
import ShowMapFilters from './ShowMapFilters';
import './ShowMapPage.scss';

const googleMapUrl = `https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=${process.env.GOOGLE_MAPS_API_KEY}`;

const GET_VENUES_EVENTS = gql`
  query Venues(
    $first: Int,
    $after: String,
    $northEastLat: Float,
    $northEastLng: Float,
    $southWestLat: Float,
    $southWestLng: Float,
    $eventsArtistsNameIcontains: String,
    $eventsStartDatetimeGte: DateTime,
    $eventsEndDatetimeLte: DateTime,
  ) {
    venues(
      first: $first,
      after: $after,
      lat_Lte: $northEastLat,
      lat_Gte: $southWestLat,
      lng_Lte: $northEastLng,
      lng_Gte: $southWestLng,
      events_Artists_Name_Icontains: $eventsArtistsNameIcontains,
      events_StartDatetime_Gte: $eventsStartDatetimeGte,
      events_EndDatetime_Lte: $eventsEndDatetimeLte,
    ) {
      edges { node {
          id, name, lat, lng,
          events(
            artists_Name_Icontains: $eventsArtistsNameIcontains,
            startDatetime_Gte: $eventsStartDatetimeGte,
            endDatetime_Lte: $eventsEndDatetimeLte,
          ) {
            edges { node {
              id, name, startDatetime, endDatetime,
              artists {
                edges { node {
                  id, name,
                } }
              }
            } }
          }
      } }
    }
  }
`;

interface IState {
  northEastLat?: number;
  northEastLng?: number;
  southWestLat?: number;
  southWestLng?: number;
  first?: number;
  after?: string;
  eventsArtistsNameIcontains?: string;
  eventsStartDatetimeGte?: string;
  eventsEndDatetimeLte?: string;
}

class ShowMapPage extends React.Component<{}, IState> {
  public state = {
    eventsArtistsNameIcontains: "",
    eventsEndDatetimeLte: moment().add(7, 'days').utc().toISOString(),
    eventsStartDatetimeGte: moment().utc().toISOString(),
    first: 50,
  }

  constructor(props: any) {
    super(props);
    this.onBoundsChanged = debounce(this.onBoundsChanged.bind(this), 250);
    this.onFiltersChanged = debounce(this.onFiltersChanged.bind(this), 250);
  }

  public onBoundsChanged(bounds: google.maps.LatLngBounds) {
    const northEast = bounds.getNorthEast();
    const southWest = bounds.getSouthWest();
    this.setState({
      ...this.state,
      northEastLat: northEast.lat(),
      northEastLng: northEast.lng(),
      southWestLat: southWest.lat(),
      southWestLng: southWest.lng(),
    });
  }

  public onFiltersChanged(filters: {}) {
    this.setState({
      ...this.state,
      ...filters,
    })
  }

  public render() {
    return (
      <div className="showmap-page">
        <div className="showmap">
          <Query query={GET_VENUES_EVENTS} variables={this.state}>
            {({ loading, error, data }) => {
              let venues = [];
              if (!loading && data) {
                venues = data.venues.edges.map((edge: any) => edge.node);
              }
              return (
                <ShowMap
                  googleMapURL={googleMapUrl}
                  loadingElement={<div style={{ height: `100%` }} />}
                  containerElement={<div style={{ height: `100%` }} />}
                  mapElement={<div style={{ height: `100%` }} />}
                  venues={venues}
                  onBoundsChanged={this.onBoundsChanged}
                />
              );
            }}
          </Query>
        </div>
        <div className="showmap-filters">
          <ShowMapFilters onFiltersChanged={this.onFiltersChanged} />
        </div>
      </div>
    );
  }
}

export default ShowMapPage;
