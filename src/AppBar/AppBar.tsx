import MUIAppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import MenuIcon from '@material-ui/icons/Menu';
import * as React from 'react';
import { Link } from 'react-router-dom';
import './AppBar.scss';

const LoginLink = (props: any) => <Link {...props} to="/login" />;
const HomeLink = (props: any) => <Link {...props} to="/" />;

class AppBar extends React.Component {
  public render() {
    return (
      <MUIAppBar position="static" className="appbar">
        <Toolbar className="toolbar">
          <IconButton color="inherit" aria-label="Menu">
            <MenuIcon />
          </IconButton>
          <Typography variant="title" color="inherit" className="title">
            <Button component={HomeLink} color="inherit">Showlister</Button>
          </Typography>
          <Button className="login-link" component={LoginLink} color="inherit">Login</Button>
        </Toolbar>
      </MUIAppBar>
    );
  };
}

export default AppBar;
