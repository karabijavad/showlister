import * as React from 'react';
import { ApolloProvider } from "react-apollo";
import { BrowserRouter as Router, Route } from 'react-router-dom';
import AppBar from './AppBar/AppBar';
import client from './client';
import './index.scss';
import Login from './Login/Login';
import ShowMapPage from './ShowMap/ShowMapPage';

class App extends React.Component {
  public render() {
    return (
      <ApolloProvider client={client}>
        <Router>
          <div className="App">
            <div className="appbar">
              <AppBar />
            </div>
            <div className="content">
              <Route path='/' exact={true} component={ShowMapPage} />
              <Route path='/login' component={Login} />
            </div>
          </div>
        </Router>
      </ApolloProvider>
    );
  }
}

export default App;
