import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { gql } from 'apollo-boost';
import * as React from 'react';
import { graphql, MutateProps } from 'react-apollo';

interface ILoginVariables {
  username: string;
  password: string;
}
interface ILoginResponse {
  login: {
    me: { id: string }
    token: string;
  }
}
const loginMutation = gql`
  mutation Login($username: String!, $password: String!) {
    login(username: $username, password: $password) {
      me { id }, token
    }
  }
`;

type IProps = MutateProps<ILoginResponse, ILoginVariables>;
interface IState {
  username: string;
  password: string;
}
class Login extends React.Component<IProps, IState> {
  public state = {
    'password': '',
    'username': '',
  }

  public constructor(props: IProps) {
    super(props);
    this.submit = this.submit.bind(this);
  }

  public handleChange = (name: string) => (event: React.FormEvent<HTMLInputElement>) => {
    this.setState({
      ...this.state,
      [name]: event.currentTarget.value,
    });
  };

  public submit = (e: React.FormEvent) => {
    e.preventDefault();
    this.props.mutate({ variables: this.state })
      .then((response) => {
        // const token = response.data.login.token;
      })
  }

  public render() {
    return (
      <div className="Login">
        <form onSubmit={this.submit}>
          <TextField
            id="username"
            label="Username"
            value={this.state.username}
            // onChange={this.handleChange('username')}
            margin="normal"
          />
          <TextField
            id="password"
            type="password"
            label="Password"
            value={this.state.password}
            // onChange={this.handleChange('password')}
            margin="normal"
          />
          <Button color="primary" type="submit">Login</Button>
        </form>
      </div>
    );
  }
}

export default graphql(loginMutation)(Login);
